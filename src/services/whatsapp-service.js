'use strict'
const moment = require('moment-timezone');
const ERRORS = require('../errors');
const Routes = require('../routes');

// setting global timezone
moment.tz.setDefault("Europe/Rome");


//const baseDomain = Constants.BASEURL;


/**
 * Send messages through WhatsApp with Dialog API - https://docs.360dialog.com/whatsapp-api/whatsapp-api
 */
class WhatsappService {

  /**
   * Create an instance of Whatsapp Dialog Service
   * @param namespace
   * @param apiKey
   */
  constructor(apiKey, namespace, cloudapi) {
    this.messages = new Routes.messages(apiKey, namespace, cloudapi);
    this.media = new Routes.media(apiKey, cloudapi);
    this.configs = new Routes.configs(apiKey, cloudapi);

  }

}

module.exports = WhatsappService;
