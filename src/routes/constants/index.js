const Messages = require('./messages.constants')

class RoutesConstants{

  static get BASEURL(){
    return "https://waba.360dialog.io/v1";
  }

  static get BASEURL_CLOUDAPI(){
    return "https://waba-v2.360dialog.io";
  }

  static get CHECKCONTACTS(){
    return '/v1/contacts'
  }

  static get MESSAGES(){
    return Messages;
  }

  static get MEDIA(){
    return '/media';
  }

  static get CONFIGS(){
    return {
      WEBHOOK: '/configs/webhook',
      TEMPLATE: '/configs/templates'
    };
  }

}

module.exports = RoutesConstants;
