const __MESSAGES = {
  API: {
    SEND: '/messages'
  },
  TYPES: {
    TEXT:'text',
    DOCUMENT: 'document',
    MEDIA: 'media',
    IMAGE: 'image',
    AUDIO: 'audio',
    STICKER: 'sticker',
    VIDEO: 'video',
    CONTACT: 'contacts',
    LOCATION: 'location',
    INTERACTIVE: 'interactive',

    TEMPLATE: 'template',
    MEDIATEMPLATE: 'template',
  }
}
class Messages {

  static get API() {
    return __MESSAGES.API;
  }

  static get TYPES() {
    return __MESSAGES.TYPES;
  }

}

module.exports = Messages;
