module.exports = {
  messages: require('./messages'),
  media: require('./media'),
  configs: require('./configs'),
}
