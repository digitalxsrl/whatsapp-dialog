const AbstractRoute = require("../abstract.route");

class ConfigsService extends AbstractRoute{

  constructor(api_key, cloudapi) {
    super(api_key, "", cloudapi);
    this._api_key = api_key;
  }

  async getWebhook(){

    let endpoint = this.CONSTANTS.CONFIGS.WEBHOOK;
    if(this.BASEURL.indexOf('waba-v2')>=0){
      endpoint = "/v1"+endpoint;
    }

    let options = {
      uri: `${this.BASEURL + endpoint}`,
      method: 'GET',
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };

    return this._request(options);
  }

  async registerWebhook(url){

    let endpoint = this.CONSTANTS.CONFIGS.WEBHOOK;
    if(this.BASEURL.indexOf('waba-v2')>=0){
      endpoint = "/v1"+endpoint;
    }

    let options = {
      uri: `${this.BASEURL + endpoint}`,
      method: 'POST',
      body: {
        url: url
      },
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };

    return this._request(options);
  }

  async registerWebhookWABA(url){

    let endpoint = this.CONSTANTS.CONFIGS.WEBHOOK;
    if(this.BASEURL.indexOf('waba-v2')>=0){
      endpoint = "/v1"+endpoint;
    }

    let options = {
      uri: `${this.BASEURL}/waba_webhook`,
      method: 'POST',
      body: {
        url: url
      },
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };

    return this._request(options);
  }

  async getTemplates(){
    let endpoint = this.CONSTANTS.CONFIGS.TEMPLATE;
    if(this.BASEURL.indexOf('waba-v2')>=0){
      endpoint = "/v1"+endpoint;
    }
    let options = {
      uri: `${this.BASEURL + endpoint}`,
      method: 'GET',
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };

    return this._request(options);
  }

  async postTemplate(template){
    let endpoint = this.CONSTANTS.CONFIGS.TEMPLATE;
    if(this.BASEURL.indexOf('waba-v2')>=0){
      endpoint = "/v1"+endpoint;
    }
    let options = {
      uri: `${this.BASEURL + endpoint}`,
      method: 'POST',
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      },
      body:template
    };

    return this._request(options);
  }

  async deleteTemplate(template_name){
    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.CONFIGS.TEMPLATE}/${template_name}`,
      method: 'DELETE',
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      },
      body: {}
    };

    return this._request(options);
  }

}

module.exports = ConfigsService;
