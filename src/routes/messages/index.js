const AbstractRoute = require('../abstract.route');
const ERRORS = require('../../errors');
const fs = require('fs');

class Messages extends AbstractRoute{

  constructor(apiKey, namespace, cloudapi) {
    super(apiKey, namespace, cloudapi);
  }

  async messageReaded(id){

    let options = {};

    if(this.BASEURL.indexOf('waba-v2')>=0){
      options = {
        uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
        method: 'POST',
        body: {
          "messaging_product": "whatsapp",
          "status": "read",
          "message_id": id
        },
        json: true,
        headers: {
          "D360-API-KEY": this.apiKey,
          "Content-Type": "application/json"
        }
      };
    }else{
      options = {
        uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}/${id}`,
        method: 'PUT',
        body: {
          status: "read"
        },
        json: true,
        headers: {
          "D360-API-KEY": this.apiKey,
          "Content-Type": "application/json"
        }
      };
    }


    return this._request(options);
  }
  /**
   * Send a simple text message
   * @param toNumber WhatsApp Number to Send
   * @param txtContent Simple text content
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendText(toNumber, txtContent, callbackUrl) {

    let preview_url = false;

    if(txtContent.indexOf('http://') >=0 || txtContent.indexOf('https://') >= 0){
      preview_url = true;
    }

    let bodyToSend = {
      preview_url: preview_url,
      recipient_type: "individual",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.TEXT,
      messaging_product: "whatsapp",
      text: {
        body: txtContent
      }
    };
    //fixme: callbackUrl

    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }

  /**
   * Send a simple text message
   * @param toNumber WhatsApp Number to Send
   * @param txtContent Simple text content
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendTextV2(toNumber, txtObj, callbackUrl) {

    let preview_url = false;

    if(((txtObj||{}).body||"").indexOf('http://') >=0 || ((txtObj||{}).body||"").indexOf('https://') >= 0){
      preview_url = true;
    }

    let bodyToSend = {
      preview_url: preview_url,
      recipient_type: "individual",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.TEXT,
      messaging_product: "whatsapp",
      text: txtObj
    };
    //fixme: callbackUrl

    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }

  /**
   * Send a template message approved by Facebook
   * @param toNumber WhatsApp Number to Send
   * @param templateName Template identifier
   * @param params Array of params
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendTextTemplate(toNumber, templateName, params, callbackUrl) {

    /*
    let contact_result = await this.checkContacts([ "+" + toNumber]);
    let filtered = contact_result.contacts.filter((el)=>{ return (el.wa_id == toNumber && el.status == 'valid') });

    if(filtered.length == 0){
      throw new Error("Invalid Contact number OR blocked number");
    }
     */

    let bodyToSend = {
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.TEMPLATE,
      messaging_product: "whatsapp",
      template: {
        namespace: this.namespace,
        language: {
          policy: "deterministic",
          code: "it"
        },
        name: templateName,
        components: [
          {
            //header,body,footer. For text template only body is valid
            type: "body",
            //subtype: Optional. Used when type is set to button.. quick_reply or url
            parameters: params
          }
        ]
      }
    };

    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }

  /**
   * Send a template message approved by Facebook
   * @param toNumber WhatsApp Number to Send
   * @param templateObj Template identifier
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendTemplate(toNumber, templateObj, callbackUrl) {

    /*
    let contact_result = await this.checkContacts([ "+" + toNumber]);
    let filtered = contact_result.contacts.filter((el)=>{ return (el.wa_id == toNumber && el.status == 'valid') });

    if(filtered.length == 0){
      throw new Error("Invalid Contact number OR blocked number");
    }
     */

    let bodyToSend = {
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.TEMPLATE,
      messaging_product: "whatsapp",
      template: {
        namespace: this.namespace,
        language: {
          policy: (templateObj.language||{}).policy || "deterministic",
          code: (templateObj.language||{}).code || "it"
        },
        name: templateObj.name,
        components: templateObj.components
      }
    };

    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }

  /**
   * Send a template message with image approved by Facebook
   * @param toNumber WhatsApp Number to Send
   * @param templateName Template identifier
   * @param media_url media_url
   * @param params Array of params
   * @param param_url String of button url
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendMediaTemplate(toNumber, templateName, media_url, params, param_url, callbackUrl) {
    /*
    let bodyToSend = {
      from: this.sender,
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.MEDIATEMPLATE,
      channel: "whatsapp",
      template_name: templateName,
      params: (params || []).map(function (e) {
        e = '"' + e + '"';
        return e;
      }).join(','),
      media_url: media_url,
      lang_code: 'it',
      callback_url: callbackUrl,
      param_url: param_url || "",
    };
     */

    let bodyToSend = {
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.TEMPLATE,
      messaging_product: "whatsapp",
      template: {
        namespace: this.namespace,
        language: {
          policy: "deterministic",
          code: "it"
        },
        name: templateName,
        components: [{
            type: "header",
            "parameters": [{
              "type": "image", //image, document e video
              "image": {
                "link": "https://link-to-your-image.jpg"
              }
            }]
          },
          {
            //header,body,footer. For text template only body is valid
            type: "body",
            //subtype: Optional. Used when type is set to button.. quick_reply or url
            parameters: [
              {
                type: "text", //text, currency, date_time, image, document, video
                text: "John"
              },
              {
                type: "text",
                text: "1234abcd"
              }
            ]
          }
        ]
      }
    };


    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }


  /**
   * Send Image
   * @param toNumber
   * @param imagePath
   * @param imageTitle
   * @param txtContent
   * @param callbackUrl
   * @returns {Promise<*>}
   */
  async sendImage(toNumber, imagePath, imageTitle, txtContent, callbackUrl) {

    let bodyToSend = {
      preview_url: true,
      recipient_type: "individual",
      messaging_product: "whatsapp",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.IMAGE, // image, document, video.. ???
      image: {
        "link": imagePath,
        "caption": txtContent
      }
    };

    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }
  /**
   * Send image
   * @param toNumber WhatsApp Number to Send
   * @param imageObj
   * @param callbackUrl
   * @returns {Promise<*>}
   */
  async sendImageV2(toNumber, imageObj, callbackUrl) {

    let bodyToSend = {
      preview_url: true,
      recipient_type: "individual",
      messaging_product: "whatsapp",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.IMAGE, // image, document, video.. ???
      image: imageObj
    };

    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }

  /**
   * Send a document
   * @param toNumber WhatsApp Number to Send
   * @param documentPath Document server path
   * @param documentTitle Document file name with extension; mandatory if @documentPath doesn't contains it
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendDocument(toNumber, documentPath, documentTitle, callbackUrl) {

    let bodyToSend = {
      preview_url: true,
      recipient_type: "individual",
      messaging_product: "whatsapp",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.DOCUMENT, // image, document, video.. ???
      document: {
        link: documentPath,
        filename: documentTitle,
        caption: documentTitle
      }
    };


    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }
  /**
   * Send a document
   * @param toNumber WhatsApp Number to Send
   * @param documentObj Document object
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendDocumentV2(toNumber, documentObj, callbackUrl) {

    let bodyToSend = {
      preview_url: true,
      recipient_type: "individual",
      messaging_product: "whatsapp",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.DOCUMENT, // image, document, video.. ???
      document: documentObj
    };


    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }

  /**
   * Send a document
   * @param toNumber WhatsApp Number to Send
   * @param videoObj Video object
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendVideo(toNumber, videoObj, callbackUrl) {

    let bodyToSend = {
      preview_url: true,
      recipient_type: "individual",
      messaging_product: "whatsapp",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.DOCUMENT, // image, document, video.. ???
      video: videoObj
    };


    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }

  /**
   * Send a video
   * @param toNumber WhatsApp Number to Send
   * @param videoPath video server path
   * @param videoTitle video file name with extension; mandatory if @documentPath doesn't contains it
   * @param videoCaption video caption
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendVideo(toNumber, videoPath, videoTitle, videoCaption, callbackUrl) {

    let bodyToSend = {
      preview_url: true,
      recipient_type: "individual",
      messaging_product: "whatsapp",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.VIDEO, // image, document, video.. ???
      document: {
        link: videoPath,
        filename: videoTitle,
        caption: videoCaption
      }
    };


    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }

  /**
   * Send a interactive message
   * @param toNumber WhatsApp Number to Send
   * @param videoPath video server path
   * @param videoTitle video file name with extension; mandatory if @documentPath doesn't contains it
   * @param videoCaption video caption
   * @param callbackUrl Callback's url
   * @returns {Promise<*>}
   */
  async sendVideo(toNumber, interactiveObj, callbackUrl) {

    /** example: https://developers.facebook.com/docs/whatsapp/api/messages#media-object **/
    let bodyToSend = {
      recipient_type: "individual",
      messaging_product: "whatsapp",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.INTERACTIVE, // image, document, video.. ???
      interactive: {
        type: interactiveObj.type,
        header: interactiveObj.header||{},
        body: interactiveObj.body||{},
        footer: interactiveObj.footer||{},
        action: interactiveObj.action||{},
      }
    };


    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }

  /**
   * Callback url
   * @param toNumber
   * @param interactiveObj
   * @returns {Promise<*>}
   */
  async sendInteractiveMessage(toNumber, interactiveObj){

    let bodyToSend = {
      recipient_type: "individual",
      messaging_product: "whatsapp",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.INTERACTIVE,
      interactive: {
        type: interactiveObj.type || 'list',
        header: interactiveObj.header,
        body: interactiveObj.body,
        footer: interactiveObj.footer,
        action: interactiveObj.action
      }
    };
    //fixme: callbackUrl

    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);

  }

  async sendLocation(toNumber, location, callbackUrl) {

    let preview_url = false;

    let bodyToSend = {
      preview_url: preview_url,
      recipient_type: "individual",
      messaging_product: "whatsapp",
      to: toNumber,
      type: this.CONSTANTS.MESSAGES.TYPES.LOCATION,
      location: location
    };
    //fixme: callbackUrl

    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.MESSAGES.API.SEND}`,
      method: 'POST',
      body: bodyToSend,
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };
    return this._request(options);
  }

  /**
   * Read an incoming message from Dialog and return an object Message
   * @param whaMsg incoming message
   * @returns {{ds_name: (*|string), media_mime: *, user_third_party_id: *, company_id: *, media_third_party_id: *, cd_type: (*|string), dt_sent: (*|moment.Moment), ds_body: *, ds_phone: *, media_url: (*|string)}|{ds_name: (*|string), media_mime: *, user_third_party_id: *, company_id: *, media_third_party_id: *, cd_type: (*|string), dt_sent: (*|moment.Moment), ds_phone: *, media_filename: string | HTMLElement | BodyInit | ReadableStream<Uint8Array>, media_url: (*|string)}|{ds_name: (*|string), user_third_party_id: *, cd_type: (*|string), dt_sent: (*|moment.Moment), ds_body: *, ds_phone: *}}
   */
  readIncoming(whaMsg) {
    switch (whaMsg.type) {
      case 'text':
        return {
          ds_body: whaMsg.body || '',
          cd_type: whaMsg.type || '',
          user_third_party_id: whaMsg.reply_to,
          dt_sent: this._moment(whaMsg.created_at, 'X'),
          ds_phone: whaMsg.from,
          ds_name: whaMsg.name || ''
        };
      case 'document':
        return {
          cd_type: whaMsg.type || '',
          user_third_party_id: whaMsg.reply_to,
          dt_sent: this._moment(whaMsg.created_at, 'X'),
          media_third_party_id: whaMsg.media_name,
          media_mime: whaMsg.mime_type,
          media_filename: whaMsg.body,
          media_url: whaMsg.media_url,
          company_id: whaMsg.comapny_id,
          ds_phone: whaMsg.from,
          ds_name: whaMsg.name || ''
        };
      case 'image':
        return {
          cd_type: whaMsg.type || '',
          media_third_party_id: whaMsg.media_name,
          user_third_party_id: whaMsg.reply_to,
          media_mime: whaMsg.mime_type,
          dt_sent: this._moment(whaMsg.created_at, 'X'),
          ds_body: whaMsg.body || '',
          media_url: whaMsg.media_url || '',
          company_id: whaMsg.comapny_id,
          ds_phone: whaMsg.from,
          ds_name: whaMsg.name || ''
        };
      default:
        throw new ERRORS.WrongType({
            ds_body: 'Tipo di messaggio non consentito',
            cd_type: 'text',
            user_third_party_id: whaMsg.reply_to,
            dt_sent: this._moment(whaMsg.created_at, 'X'),
            ds_phone: whaMsg.from,
            ds_name: whaMsg.name || ''
          },
          'Unsupported Type');
    }
  }

}

module.exports = Messages;
