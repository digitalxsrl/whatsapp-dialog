//const azureStorage = require("azure-storage");
const { BlockBlobClient, BlobServiceClient, StorageSharedKeyCredential } = require("@azure/storage-blob");
const AbstractRoute = require("../abstract.route");
const streamifier = require("streamifier");
const {https} = require("follow-redirects");
const Axios = require('axios');
const uuidV4 = require('uuid').v4;

class MediaService extends AbstractRoute{

  constructor(api_key, cloudapi) {
    super(api_key, "", cloudapi);
    this._api_key = api_key;
  }

  async uploadOnMeta(binaryData, contentType){
    let options = {};

    if(this.cloudapi){
      /*
      let formData = new FormData();
      formData.append('messaging_product', 'whatsapp');
      formData.append('file', binaryData);

      let config = {
        method: 'post',
        url: `${this.BASEURL+this.CONSTANTS.MEDIA}`,
        headers: {
          'D360-API-KEY': this.apiKey,
          'Content-Type': 'multipart/form-data'
        },
        data : formData
      };*/

      options = {
        'method': 'POST',
        'url': `${this.BASEURL+this.CONSTANTS.MEDIA}`,
        'headers': {
          'D360-API-KEY': this.apiKey
        },
        formData: {
          'file': {
            'value': binaryData,
            'options': {
              'filename': uuidV4(),
              'contentType': contentType
            }
          },
          'messaging_product': 'whatsapp'
        }
      };

      let response = await this._request(options);
      response = JSON.parse(response);
      return JSON.stringify({"media":[{"id":response.id}],"meta":{"api_status":"stable","version":"2.51.2"}});

    }else{

      options = {
        uri: `${this.BASEURL+this.CONSTANTS.MEDIA}`,
        method: 'POST',
        body: binaryData,
        headers: {
          "D360-API-KEY": this.apiKey,
          "Content-Type": contentType
        }
      };
    }

    return this._request(options);

  }

  async uploadMediaFromId(fileid){
    let _this = this;
    let https = require('follow-redirects').https;

    return new Promise(async (resolve, reject)=>{

      let endpoint = this.BASEURL+this.CONSTANTS.MEDIA+"/"+fileid;

      if(_this.cloudapi){
        endpoint = this.BASEURL+"/"+fileid;

        let  options = {
          uri: endpoint,
          method: 'GET',
          headers: {
            "D360-API-KEY": this.apiKey
          }
        };
        let response = await this._request(options);

        try{
          response = JSON.parse(response);
        }catch (e){}

        const response_2 = await Axios({
          url: response.url.replace('https://lookaside.fbsbx.com', this.BASEURL),
          method: 'GET',
          headers:{
            "D360-API-KEY": this.apiKey
          },
          responseType: 'arraybuffer'
        });

        resolve(await _this.uploadMedia(fileid, response_2.data, response.mime_type, response_2.headers["content-length"]));


      }else{

        let myreq = https.request(endpoint, {
          headers: {
            'D360-API-KEY': _this._api_key
          }
        }, function(res){
          let chunks = [];

          res.on("data", function (chunk) {
            chunks.push(chunk);
          });

          res.on("end", async function (chunk) {
            let body = Buffer.concat(chunks);
            try{
              resolve(await _this.uploadMedia(fileid, body, res.headers["content-type"]));
            }catch (e){
              reject(e);
            }
          });

          res.on("error", function (error) {
            reject(error);
          });
        });

        myreq.end();

      }

    });
  }

  async uploadMedia(orignal_filename, filedata, content_type){

    const accountname = "etistoragefunctions";
    const key = "XOzWfQq0Cuz+vXsrnvqfj5NIMosA8pjUKv7SL9TAem+2MxmwmjZfiP/k4vEXqGVmOKB78U6497tRRAvd1MnxCw==";
    const containerName = "files";
    const prefix = "pingueen";

    /*
    let blobClient  = azureStorage.createBlobService(accountname,key);
    let options = {
      contentSettings:{
        contentType: content_type
      }
    };
     */
    const blobEndpoint = `https://${accountname}.blob.core.windows.net`;
    const blobService = new BlobServiceClient(
        blobEndpoint,
        new StorageSharedKeyCredential(accountname, key)
    );

    let filename = orignal_filename;

    return new Promise(async(resolve, reject)=>{

      try{
        await blobService.createContainer(containerName);
      }catch (e){}

      const blobUrl = `https://${accountname}.blob.core.windows.net/${prefix}/${filename}`;
      const blockBlobClient = new BlockBlobClient(
          blobUrl,
          new StorageSharedKeyCredential(accountname, key)
      );
      const blobOptions = { blobHTTPHeaders: { blobContentType: content_type } };
      try{
        await blockBlobClient.uploadStream(streamifier.createReadStream(filedata), filedata.length, 1, blobOptions)
        resolve({
          file_url: `https://media.etihub.it/${prefix}/${filename}`,
          _id: filename,
          filename:orignal_filename,
          contentType: content_type
        });

      }catch (e){
        return reject({"error": "Blob error"});
      }

      /*
      blobClient.createContainerIfNotExists(prefix, {
        publicAccessLevel: 'blob'
      }, function(error_container, result, response) {
        if (error_container) {
          return reject({"error": "Blob error"});
        }

        blobClient.createBlockBlobFromStream(prefix, filename, streamifier.createReadStream(filedata), filedata.length, options, (err, result) => {
          if(err){
            reject({"error": "Blob error"});
          }else{
            resolve({
              file_url: `https://media.etihub.it/${prefix}/${filename}`,
              _id: filename,
              filename:orignal_filename,
              contentType: content_type
            });
          }
        });

      });
      */
    });

  }

}

module.exports = MediaService;
