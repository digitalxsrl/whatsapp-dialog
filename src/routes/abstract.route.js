const RoutesConstants = require('./constants');
const moment = require('moment-timezone');
const request = require('request-promise-native');

class AbstractRoute {

  constructor(apiKey, namespace, cloudapi) {
    this.apiKey = apiKey;
    this.namespace = namespace;
    this.cloudapi = !!cloudapi;

    this.CONSTANTS =  RoutesConstants;
    this.BASEURL = !!cloudapi ? this.CONSTANTS.BASEURL_CLOUDAPI : this.CONSTANTS.BASEURL;
    this._request =  request;
    this._moment =  moment;

  }

  async checkContact(number){

    if(number.indexOf('+') !== 0){
      number = "+"+number;
    }

    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.CHECKCONTACTS}`,
      method: 'POST',
      body: {
        blocking :  "wait",
        contacts: [number],
        force_check: true
      },
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };


    let response = await this._request(options);
    let filterd = (response.contacts||[]).filter((el)=>{ return (el.input==number && el.status == 'valid')});

    if(filterd.length == 0){
      throw new Error("Invalid contact or blocked");
    }

    return filterd;

  }

  async checkContacts(list = [], blocking = "wait"){

    return new Promise(function(resolve, reject) {
      try{
        resolve([{
          "wa_id": list[0].replace('+',''),
          "input": list[0].replace('+',''),
          "status": "valid"
        }])
      }catch (e){
        reject(e);
      }
    })
    /*
    let options = {
      uri: `${this.BASEURL + this.CONSTANTS.CHECKCONTACTS}`,
      method: 'POST',
      body: {
        blocking : blocking,
        contacts: list,
        force_check: true
      },
      json: true,
      headers: {
        "D360-API-KEY": this.apiKey,
        "Content-Type": "application/json"
      }
    };


    if(blocking === "blocking"){
      let response = await this._request(options);
      //return only valid numbers
      return (response.contacts||[]).filter((el)=>{ return el.status == 'valid' });
    }else{
      return this._request(options);
    }
     */

  }

}

module.exports = AbstractRoute;
